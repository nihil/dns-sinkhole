curl https://api.hackertarget.com/hostsearch/?q=googlevideo.com \
| sed 1d \
| cut -f 1 -d ',' \
| awk '{ printf "0.0.0.0 %s\n", $1 }'
